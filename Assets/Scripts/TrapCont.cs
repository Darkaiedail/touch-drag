﻿using UnityEngine;
using System.Collections;

public class TrapCont : MonoBehaviour {

	private GameObject character;

	public int mainLavel;
	public GameObject restartButton;

	void Start(){
		character = GameObject.FindGameObjectWithTag("Player");
	}

	void OnTriggerStay(Collider col){
		if(col.gameObject.CompareTag("Player")){
			Destroy(character.gameObject);
			restartButton.SetActive(true);
		}
	}

	public void restart(){
		Application.LoadLevel(mainLavel);
	}
}
