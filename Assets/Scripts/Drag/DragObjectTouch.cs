﻿using UnityEngine;
using System.Collections;

public class DragObjectTouch : MonoBehaviour {


	private float maxPickingDistance = 10000;// increase if needed, depending on your scene size
	
	//private Vector3 startPos;
	
	private Transform pickedObject = null;


	public Vector3 movePosition = Vector3.zero;
	
	// Update is called once per frame
	void Update () {


		foreach (Touch touch in Input.touches) {

			//Gets the ray at position where the screen is touched
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			
			if (touch.phase == TouchPhase.Began) {

				RaycastHit hit = new RaycastHit();

				if (Physics.Raycast(ray, out hit, maxPickingDistance)) { 

					if(hit.transform.gameObject.CompareTag("Box") || hit.transform.gameObject.CompareTag("Stairs")){

						pickedObject = hit.transform;

						Vector3 cameraTransform = Camera.main.transform.InverseTransformPoint(0, 0, 0);

						pickedObject.position = Camera.main.ScreenToWorldPoint( new Vector3 (touch.position.x, touch.position.y, cameraTransform.z - 0.5f));
					}
				} 
				else{
					pickedObject = null;
				}
			} 
			else if (touch.phase == TouchPhase.Moved){

				if (pickedObject != null){

					Vector3 cameraTransform = Camera.main.transform.InverseTransformPoint(0, 0, 0);
						
					pickedObject.position = Camera.main.ScreenToWorldPoint( new Vector3 (touch.position.x, touch.position.y, cameraTransform.z - 0.5f));

				}
			} 
			else if (touch.phase == TouchPhase.Ended) {
				pickedObject = null;
			}
		}
	}
}