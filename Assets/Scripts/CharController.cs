﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {

	//movement
	public float runSpeed = 8f;
	private float groundDamping = 20f; //smooth

	private float normalizedHorizontalSpeed = 0;
	public float jumpHigh;
	Vector2 speed = Vector2.zero;

	//startRound
	private bool startRound = false;

	private GameObject box;
	private GameObject stairs;

	//upStairs
	private bool goUpStairs;

	//restart level
	public int mainLavel;
	public GameObject restartButton;

	void Update(){

		box = GameObject.FindGameObjectWithTag("Box");
		stairs = GameObject.FindGameObjectWithTag("Stairs");

		if(startRound){
			speed.y = 0;
			speed.x = .1f;
			moveX();

			if(Input.GetKeyDown(KeyCode.Space)){
				speed.y = 1;
				moveY();
			}

			//Check if Input has registered more than zero touches
			if(Input.touchCount > 0){
				//Store the first touch detected
				Touch myTouch = Input.touches[0];
	
				//begining of the touch on the screen
				if(myTouch.phase == TouchPhase.Began){
					speed.y = 1;
					moveY();
				}
			}
		}

		if(!startRound && goUpStairs){
			this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
			this.gameObject.GetComponent<Rigidbody>().useGravity = false;
			speed.x = 0;
			speed.y = .05f;
			moveY();
		}
	}

	void moveX(){
		var smoothedMovementFactor = groundDamping;

		speed.x = Mathf.Lerp(speed.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

		transform.Translate(speed, Space.World);
	}

	void moveY(){
		transform.Translate(speed * jumpHigh * Time.deltaTime,Space.World);
	}

	public void upStairs(){
		startRound = false;

		StartCoroutine("beginGoUp");
	}

	IEnumerator beginGoUp(){
		yield return new WaitForSeconds(1.0f);

		goUpStairs = true;
	}

	public void stopUp(){
		goUpStairs = false;
		this.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		startRound = true;
		StartCoroutine("restartGravity");
	}

	IEnumerator restartGravity(){
		yield return new WaitForSeconds(.3f);

		this.gameObject.GetComponent<Rigidbody>().useGravity = true;
	}

	public void beginRound(){
		StartCoroutine("begin");
	}

	IEnumerator begin(){
		yield return new WaitForSeconds(3.0f);

		//all objects are kinematic
		box.GetComponent<Rigidbody>().isKinematic = true;
		box.GetComponent<DragObjectMouse>().enabled = false;

		stairs.GetComponent<Rigidbody>().isKinematic = true;
		stairs.GetComponent<Collider>().isTrigger = true;
		stairs.GetComponent<DragObjectMouse>().enabled = false;

		startRound = true;
	}

	public void restart(){
		Application.LoadLevel(mainLavel);
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.CompareTag("FinalCollider")){
			restartButton.SetActive(true);
			startRound = false;
		}
	}
}
