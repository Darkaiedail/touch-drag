﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

	//gameObjects
	public GameObject box;
	public GameObject stairs;
	public GameObject button;

	//destroy walls
	public bool canDestroy;

	//limited instances
	public int numBoxes;
	public Text numBoxesTxt;
	public int numStairs;
	public Text numStairsTxt;
	public int numDestroyableWalls;
	public Text numDestroyableWallsTxt;

	public void instantiateBox () {
		numBoxes --;
		if(numBoxes > 0){
			Instantiate(box, Vector3.zero, Quaternion.identity);
		}
		else{
			numBoxes = 0;
		}
	}

	public void instantiateStairs () {
		numStairs --;
		if(numStairs > 0){
			Instantiate(stairs, Vector3.zero, Quaternion.identity);
		}
		else{
			numStairs = 0;
		}
	}

	public void instantiateButton () {
		Instantiate(button, Vector3.zero, Quaternion.identity);
	}

	public void destroyWalls(){
			canDestroy = true;
	}

	void Update(){
		if(canDestroy){
			//Check if Input has registered more than zero touches
			if(Input.touchCount > 0){
				//Store the first touch detected
				Touch myTouch = Input.touches[0];

				Vector3 pos = myTouch.position;

				//begining of the touch on the screen
				if(myTouch.phase == TouchPhase.Began){
					RaycastHit hit;
					Ray ray = Camera.main.ScreenPointToRay(pos); 
					if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.CompareTag ("Wall")){
						Destroy(hit.collider.gameObject);
					}
				}
			}

			//configuracion del rayo
			Ray rayTwo = Camera.main.ScreenPointToRay (Input.mousePosition);
			//lo que trae el rayo de vuelta
			RaycastHit hitTwo = new RaycastHit ();

			if (Physics.Raycast (rayTwo, out hitTwo, 1000)) {

				if(Input.GetMouseButtonDown(0) && hitTwo.collider.gameObject.CompareTag ("Wall")){//1 bot dcho, 0 bot izdo, 3 bot centro
					numDestroyableWalls --;

					if(numDestroyableWalls > 0){
						Debug.DrawLine(Camera.main.transform.position, hitTwo.point, Color.red);//pongo hit.point xq qiero q me devuelva la pos de dond incide el rayo, si pongo hit.collider.transform.position me devolveria el cdg de la figura
						Destroy(hitTwo.collider.gameObject);
					}
					else{
						numDestroyableWalls = 0;
					}

				}
			}
		}

		remBox();
		remStairs();
		remWalls();
	}

	void remBox(){
		int remainingBoxes = numBoxes - 1;

		if(remainingBoxes >= 0){
			string numBoxesStr = remainingBoxes.ToString("0");
			numBoxesTxt.text = numBoxesStr;
		}
		else{
			remainingBoxes = 0;
		}
	}

	void remStairs(){
		int remainingStairs = numStairs - 1;

		if(remainingStairs >= 0){
			string numStairsStr = remainingStairs.ToString("0");
			numStairsTxt.text = numStairsStr;
		}
		else{
			remainingStairs = 0;
		}
	}

	void remWalls(){
		int remainingWalls = numDestroyableWalls - 1;

		if(remainingWalls >= 0){
			string numWallsStr = remainingWalls.ToString("0");
			numDestroyableWallsTxt.text = numWallsStr;
		}
		else{
			remainingWalls = 0;
		}
	}
}
