﻿using UnityEngine;
using System.Collections;

public class DoorCont : MonoBehaviour {

	private bool nextLevel;
	public int levelNum;

	public void openDoor(){
		nextLevel = true;
	}

	void OnTriggerEnter (Collider col) {
		if(nextLevel && col.gameObject.CompareTag("Player")){
			Application.LoadLevel(levelNum);
		}	
	}
}
