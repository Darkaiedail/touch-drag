﻿using UnityEngine;
using System.Collections;

public class StairsCont : MonoBehaviour {

	private GameObject character;

	void Start(){
		character = GameObject.FindGameObjectWithTag("Player");
	}

	void OnTriggerEnter(Collider col){
		if(col.gameObject.CompareTag("Player")){
			character.GetComponent<CharController>().upStairs();

		}
	}

	void OnTriggerExit(Collider col){
		if(col.gameObject.CompareTag("Player")){
			character.GetComponent<CharController>().stopUp();

		}
	}
}
