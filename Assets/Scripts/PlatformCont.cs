﻿using UnityEngine;
using System.Collections;

public class PlatformCont : MonoBehaviour {

	public float moveX;
	public float moveY;
	private bool movePlatform;

	void movePlatformFinalPosition(){
		iTween.MoveAdd(this.gameObject, iTween.Hash("x",moveX,
													"y", moveY));
	}

	void Update () {

		if(!movePlatform){
			if(Input.touchCount > 0){
				//Store the first touch detected
				Touch myTouch = Input.touches[0];

				Vector3 pos = myTouch.position;

				//begining of the touch on the screen
				if(myTouch.phase == TouchPhase.Began){
					RaycastHit hit;
					Ray ray = Camera.main.ScreenPointToRay(pos); 
					if(Physics.Raycast(ray, out hit) && hit.collider.gameObject.CompareTag ("Platform")){
						movePlatformFinalPosition();
						movePlatform = true;
					}
				}
			}

			//configuracion del rayo
			Ray rayTwo = Camera.main.ScreenPointToRay (Input.mousePosition);
			//lo que trae el rayo de vuelta
			RaycastHit hitTwo = new RaycastHit ();

			if (Physics.Raycast (rayTwo, out hitTwo, 1000)) {

				if(Input.GetMouseButtonDown(0) && hitTwo.collider.gameObject.CompareTag ("Platform")){//1 bot dcho, 0 bot izdo, 3 bot centro
					Debug.DrawLine(Camera.main.transform.position, hitTwo.point, Color.red);//pongo hit.point xq qiero q me devuelva la pos de dond incide el rayo, si pongo hit.collider.transform.position me devolveria el cdg de la figura
					movePlatformFinalPosition();
					movePlatform = true;
				}
			}
		}
	}
}
